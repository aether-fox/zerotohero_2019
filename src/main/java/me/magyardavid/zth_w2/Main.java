package me.magyardavid.zth_w2;

import me.magyardavid.zth_w2.util.*;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.lang.Double.*;
import static me.magyardavid.zth_w2.util.Direction.FOUR_DIRECTIONS;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
@SuppressWarnings("WeakerAccess")
public class Main {

    private static final Coordinates POLICE_COORDINATES = new Coordinates(0, 0);

    public static void main(final@Nonnull String[] args) {
        final @Nonnull Matrix<HouseData> houseData = loadHouseData("map.csv");
        final @Nonnull DoubleMatrix houseWeights = collectHouseWeights(houseData);
        final @Nonnull Coordinates[] suspiciousHouses = findSuspiciousHouses(houseData).toArray(new Coordinates[0]);
        if (suspiciousHouses.length == 0) {
            System.out.println("Nincsenek gyanús házak, nincs mit tenni.");
            return;
        } else {
            for (final @Nonnull Coordinates suspiciousHouseCoordinate : suspiciousHouses)
                System.out.println("Gyanús ház: [" + suspiciousHouseCoordinate.getX() + ":" + suspiciousHouseCoordinate.getY() + "]");
        }

        helpPolice(suspiciousHouses, houseWeights);
    }

    private static @Nonnull DoubleMatrix collectHouseWeights(final @Nonnull Matrix<HouseData> houseData) {
        return houseData.mapToDouble(HouseData::getWeight);
    }

    @SuppressWarnings("SameParameterValue")
    private static @Nonnull Matrix<HouseData> loadHouseData(final @Nonnull String fileName) {
        final @Nonnull File file = new File(fileName);
        if (!file.exists())
            throw new IllegalArgumentException("File not found! (" + file.getAbsolutePath() + ")");

        try (final @Nonnull CSVReader reader = CSVReader.open(file, ';')) {
            final @Nonnull ArrayList<HouseData> allHouseData = new ArrayList<>();

            @Nonnegative int rowLength = 1; //1 because of the police!!!
            boolean rowLengthKnown = false;

            //discard header line
            reader.next();

            while (reader.hasNext()) {
                @Nonnull String houseLine$ = reader.next();

                if (houseLine$.startsWith("\n")) {
                    houseLine$ = houseLine$.substring("\n".length());
                    if (!rowLengthKnown)
                        rowLengthKnown = true;

                } else if (!rowLengthKnown)
                    ++rowLength;

                allHouseData.add(HouseData.parse(houseLine$));
            }

            if (allHouseData.size() % rowLength != 0)
                throw new IllegalStateException("The number of houses does not fit with the grid of rows with length: " + rowLength);

            final @Nonnegative int houseCount = allHouseData.size();
            final @Nonnull Matrix<HouseData> matrix = new Matrix<>(rowLength, houseCount / rowLength);
            for (@Nonnegative int houseIdx$ = 0; houseIdx$ < houseCount; ++houseIdx$)
                matrix.set(houseIdx$ % rowLength, houseIdx$ / rowLength, allHouseData.get(houseIdx$));

            return matrix;
        } catch (final @Nonnull IOException e) {
            throw new IllegalStateException("Some IO error occurred during house data read.", e);
        } catch (final @Nonnull Exception e) {
            throw new IllegalStateException("Some error occurred during house data read.", e);
        }
    }

    private static @Nonnull List<Coordinates> findSuspiciousHouses(final @Nonnull Matrix<HouseData> houseData) {
        final @Nonnull ArrayList<Coordinates> suspiciousHouseCoordinates = new ArrayList<>();

        final @Nonnegative int width = houseData.getWidth();
        final @Nonnegative int heigth = houseData.getHeight();

        for (int y$ = 0; y$ < heigth; ++y$)
            for (int x$ = 0; x$ < width; ++x$) {
                final @Nonnull HouseData house = houseData.get(x$, y$);
                if (house.getColor() == HouseColor.RED && house.getLevels() == (byte) 2 && house.getType() == HouseType.KERTES && house.getFence() == FenceType.HIGH)
                    suspiciousHouseCoordinates.add(new Coordinates(x$, y$));
            }

        return suspiciousHouseCoordinates;
    }

    private static void helpPolice(final @Nonnull Coordinates[] suspiciousHouses, final @Nonnull DoubleMatrix weights) {
        final @Nonnull DoubleMatrix shortestPathsFromPolice = calculateShortestPaths(weights, POLICE_COORDINATES.getX(), POLICE_COORDINATES.getY());

        final @Nonnull double[] distancesToHousesFromPolice = new double[suspiciousHouses.length];
        final @Nonnull DoubleMatrix distancesBetweenHouses = DoubleMatrix.createNew(suspiciousHouses.length, suspiciousHouses.length);

        for (@Nonnegative int houseIndex$ = 0; houseIndex$ < suspiciousHouses.length; ++houseIndex$) {
            distancesToHousesFromPolice[houseIndex$] = distance(POLICE_COORDINATES, suspiciousHouses[houseIndex$], shortestPathsFromPolice);
            final @Nonnull DoubleMatrix shortestPathsFromHouse = calculateShortestPaths(weights, suspiciousHouses[houseIndex$].getX(), suspiciousHouses[houseIndex$].getY());
            for (@Nonnegative int otherHouseIndex$ = 0; otherHouseIndex$ < suspiciousHouses.length; ++otherHouseIndex$) {
                distancesBetweenHouses.put(houseIndex$, otherHouseIndex$, distance(suspiciousHouses[houseIndex$], suspiciousHouses[otherHouseIndex$], shortestPathsFromHouse));
            }
        }

        findOptimalTraversals(suspiciousHouses, shortestPathsFromPolice);
    }

    private static void findOptimalTraversals(
            final @Nonnull Coordinates[] suspiciousHouses,
            final @Nonnull DoubleMatrix shortestPathsFromPolice) {

        double minDist$ = NaN;
        int closestSuspiciousHouseIdx$ = -1;
        for (@Nonnegative int suspiciousHouseIdx$ = 0; suspiciousHouseIdx$ < suspiciousHouses.length; ++suspiciousHouseIdx$) {
            final int dX = suspiciousHouses[suspiciousHouseIdx$].getX() - POLICE_COORDINATES.getX();
            final int dY = suspiciousHouses[suspiciousHouseIdx$].getX() - POLICE_COORDINATES.getY();
            final double distance = Math.sqrt(dX * dX + dY * dY);
            if (isNaN(minDist$) || distance < minDist$) {
                minDist$ = distance;
                closestSuspiciousHouseIdx$ = suspiciousHouseIdx$;
            }
        }

        final @Nonnull Optional<List<Coordinates>> path = getPath(
                POLICE_COORDINATES,
                suspiciousHouses[closestSuspiciousHouseIdx$],
                shortestPathsFromPolice);

        if (path.isPresent()) {
            System.out.println("Útvonal a házig:");
            for (final @Nonnull Coordinates $coordinates : path.get())
                System.out.println("  - " + $coordinates.getX() + ":" + $coordinates.getY());
        } else
            System.out.println("A ház nem érhető el a rendőségről!");
    }

    /**
     * Returns the path where from the starting location the target location can be reached.
     * <p>The algorithm traverses the shortest path backwards from target location to start location,
     * but the returned list contains the path in the right direction - from start to target location.</p>
     */
    public static @Nonnull Optional<List<Coordinates>> getPath(
            final @Nonnull Coordinates startLoc,
            final @Nonnull Coordinates targetLoc,
            final @Nonnull DoubleMatrix distances) {

        if (distances.get(startLoc.getX(), startLoc.getY()) != 0.0)
            throw new IllegalArgumentException("Start points should be exactly 0 units away!");

        final @Nonnull ArrayList<Coordinates> path = new ArrayList<>();

        final @Nonnegative int width = distances.getWidth();
        final @Nonnegative int height = distances.getHeight();
        @Nonnegative int x$ = targetLoc.getX();
        @Nonnegative int y$ = targetLoc.getY();

        while (x$ != startLoc.getX() || y$ != startLoc.getY()) {
            double minDistance$ = POSITIVE_INFINITY;
            int bestX$ = -1, bestY$ = -1;
            for (final @Nonnull Direction $dir : FOUR_DIRECTIONS) {
                int aroundX = x$ + $dir.getModX();
                int aroundY = y$ + $dir.getModY();
                if (aroundX < 0 || aroundY < 0 || aroundX == width || aroundY == height)
                    continue;

                final double distance = distances.get(aroundX, aroundY);
                if (distance < minDistance$) {
                    minDistance$ = distance;
                    bestX$ = aroundX;
                    bestY$ = aroundY;
                }
            }

            if (!isFinite(minDistance$))
                return Optional.empty();

            x$ = bestX$;
            y$ = bestY$;

            path.add(new Coordinates(bestX$, bestY$));
        }

        Collections.reverse(path);
        return Optional.of(path);
    }

    public static double distance(
            final @Nonnull Coordinates startLoc,
            final @Nonnull Coordinates targetLoc,
            final @Nonnull DoubleMatrix distances) {

        if (distances.get(startLoc.getX(), startLoc.getY()) != 0.0)
            throw new IllegalArgumentException("Start points should be exactly 0 units away!");

        return distances.get(targetLoc.getX(), targetLoc.getY());
    }

    /**Returns a map of shortest distances to other coordinates from the given starting location.
     * <p>The returned map contains {@link Double#NaN} at places which are unreachable.</p>
     * <p>Unreachable places might occur due to not finite weights.</p>*/
    public static @Nonnull DoubleMatrix calculateShortestPaths(
            final @Nonnull DoubleMatrix weights,
            final @Nonnegative int startX,
            final @Nonnegative int startY) {

        final @Nonnegative int width = weights.getWidth();
        final @Nonnegative int height = weights.getHeight();

        final @Nonnull DoubleMatrix shortestPaths = DoubleMatrix.createNew(width, height);
        @Nonnull BitMatrix updateFlagsRecent$ = new BitMatrix(width, height);
        @Nonnull BitMatrix updateFlagsCurrent$ = new BitMatrix(width, height);
        updateFlagsRecent$.setAll(false);
        updateFlagsCurrent$.setAll(false);

        shortestPaths.setAll(NaN);
        shortestPaths.put(startX, startY, 0.0);
        updateFlagsRecent$.set(startX, startY, true);

        boolean updatedAtLeastOne;
        do {
            updateFlagsCurrent$.setAll(false);
            updatedAtLeastOne = false;
            for (@Nonnegative int y$ = 0; y$ < height; ++y$) {
                for (@Nonnegative int x$ = 0; x$ < width; ++x$) {
                    if (updateFlagsRecent$.get(x$, y$)) {
                        for (final @Nonnull Direction dir : FOUR_DIRECTIONS) {
                            final int xAround = x$ + dir.getModX();
                            final int yAround = y$ + dir.getModY();
                            if (xAround < 0 || xAround == width || yAround < 0 || yAround == height)
                                continue;

                            final double currentValue = shortestPaths.get(xAround, yAround);
                            final double newValue = shortestPaths.get(x$, y$) + weights.get(xAround, yAround);
                            if (isNaN(currentValue) || newValue < currentValue) {
                                shortestPaths.put(xAround, yAround, newValue);
                                updateFlagsCurrent$.set(xAround, yAround, true);
                                updatedAtLeastOne = true;
                            }
                        }
                    }
                }
            }

            final @Nonnull BitMatrix helper = updateFlagsRecent$;
            updateFlagsRecent$ = updateFlagsCurrent$;
            updateFlagsCurrent$ = helper;
        } while (updatedAtLeastOne);

        return shortestPaths;
    }

    @Deprecated
    private static void fillWithShortestPath(final @Nonnull DoubleMatrix shortestPaths, final @Nonnull DoubleMatrix weights) {
        final @Nonnull ArrayList<DoubleMatrix.Value> newValues = new ArrayList<>();
        do {
            newValues.clear();
            for (int x$ = 0; x$ < 10; ++x$)
                for (int y$ = 0; y$ < 10; ++y$)
                    if (shortestPaths.get(x$, y$) == 0.0) {

                        double min$ = 0.0;

                        if (x$ != 0) min$ = shortestPaths.get(x$-1, y$);
                        if (y$ != 0) min$ = Math.min(min$, shortestPaths.get(x$, y$-1));
                        if (x$ != 9) min$ = Math.min(min$, shortestPaths.get(x$+1, y$));
                        if (y$ != 9) min$ = Math.min(min$, shortestPaths.get(x$, y$+1));

                        if (min$ != 0.0) {
                            //resolved
                            final@Nonnegative double weight = weights.get(x$, y$);
                            final @Nonnull DoubleMatrix.Value val = new DoubleMatrix.Value();
                            val.x = x$;
                            val.y = y$;
                            val.value = min$ + weight;
                            newValues.add(val);
                        }
                    }
            for (@Nonnull DoubleMatrix.Value value : newValues)
                shortestPaths.put(value.x, value.y, value.value);
        } while (!newValues.isEmpty());
    }

}
