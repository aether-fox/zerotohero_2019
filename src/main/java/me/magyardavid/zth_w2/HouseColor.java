package me.magyardavid.zth_w2;

import javax.annotation.Nonnull;

public enum HouseColor {
    YELLOW,
    BLUE,
    BROWN,
    GREEN,
    ORANGE,
    RED,
    PINK,
    WHITE,
    BLACK;

    public static HouseColor parse(final @Nonnull String string) {
        switch (string.toUpperCase()) {
            case "KÉK":
                return BLUE;
            case "ZÖLD":
                return GREEN;
            case "SÁRGA":
                return YELLOW;
            case "BARNA":
                return BROWN;
            case "NARANCS":
            case "NARANCSSÁRGA":
                return ORANGE;
            case "PIROS":
            case "VÖRÖS":
                return RED;
            case "RÓZSASZÍN":
                return PINK;
            case "FEHÉR":
                return WHITE;
            case "FEKETE":
                return BLACK;
            default:
                return valueOf(string);
        }
    }
}
