package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.util.function.ToDoubleFunction;

/**
 * All rights reserved.
 *
 * @author Magyar Dávid
 * @since 2019. 08. 25.
 */
public class Matrix<T> {

    private final @Nonnull Object[] values;

    private final @Nonnegative int width;

    public Matrix(final @Nonnegative int width, final @Nonnegative int height) {
        this.width = width;
        this.values = new Object[width * height];
    }

    public void set(final @Nonnegative int x, final @Nonnegative int y, final T value) {
        values[y * width + x] = value;
    }

    public T get(final @Nonnegative int x, final @Nonnegative int y) {
        //noinspection unchecked
        return (T) values[y * width + x];
    }

    public @Nonnegative int getWidth() {
        return width;
    }

    public @Nonnegative int getHeight() {
        return values.length / width;
    }

    public @Nonnull DoubleMatrix mapToDouble(final @Nonnull ToDoubleFunction<T> mapper) {
        final @Nonnegative int height = getHeight();

        final @Nonnegative DoubleMatrix doubleMatrix = DoubleMatrix.createNew(width, height);
        for (int y$ = 0; y$ < height; ++y$) {
            for (int x$ = 0; x$ < width; ++x$) {
                doubleMatrix.put(x$, y$, mapper.applyAsDouble(get(x$, y$)));
            }
        }

        return doubleMatrix;
    }

}
