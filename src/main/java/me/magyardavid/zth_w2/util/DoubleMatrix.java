package me.magyardavid.zth_w2.util;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

public interface DoubleMatrix {

    class Value {
        public int x, y;
        public double value;
    }

    @Nonnull double[] getRawData();

    @Nonnegative int getWidth();

    @Nonnegative int getHeight();

    static DoubleMatrix of(final @Nonnull double[] data, final @Nonnegative int width, final @Nonnegative int height) {
        if (data.length != width * height)
            throw new IllegalArgumentException("Array of "
                    + data.length + " cannot be a 2D array of "
                    + width + "*"
                    + height + " values$");

        return new DoubleMatrix() {
            @Override
            public double get(final@Nonnegative int x, final@Nonnegative int y) {
                return data[y * width + x];
            }

            @Override
            public double[] getRawData() {
                return data;
            }

            @Override
            public double put(final@Nonnegative int x, final@Nonnegative int y, final double newValue) {
                final@Nonnegative int index = y * width + x;
                final double oldValue = data[index];
                data[index] = newValue;
                return oldValue;
            }

            public @Nonnull DoubleMatrix copy() {
                final@Nonnull double[] copiedData = new double[data.length];
                System.arraycopy(data, 0, copiedData, 0, copiedData.length);
                return DoubleMatrix.of(copiedData, width, height);
            }

            @Override
            public int getWidth() {
                return width;
            }

            @Override
            public int getHeight() {
                return height;
            }
        };
    }

    static @Nonnull DoubleMatrix createNew(final @Nonnegative int width, final @Nonnegative int height) {
        final@Nonnull double[] data = new double[width * height];
        return of(data, width, height);
    }

    double get(final @Nonnegative int x, final @Nonnegative int y);

    double put(final @Nonnegative int x, final @Nonnegative int y, final double value);

    default void setAll(final double value) {
        final @Nonnull double[] rawData = getRawData();
        for (@Nonnegative int i$ = 0; i$ < rawData.length; ++i$) {
            rawData[i$] = value;
        }
    }
}
