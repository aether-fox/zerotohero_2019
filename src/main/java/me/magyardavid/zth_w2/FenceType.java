package me.magyardavid.zth_w2;

import javax.annotation.Nonnull;

public enum FenceType {
    NONE,
    LOW,
    HIGH;

    public static @Nonnull FenceType parse(final @Nonnull String string) {
        switch (string.toUpperCase()) {
            case "MAGAS":
            case "MAGAS KERÍTÉS":
                return HIGH;
            case "ALACSONY":
            case "ALACSONY KERÍTÉS":
                return LOW;
            case "NINCS":
            case "NINCS KERÍTÉS":
                return NONE;
            default:
                return valueOf(string);
        }
    }

}
