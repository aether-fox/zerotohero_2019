package me.magyardavid.zth_w2;

import javax.annotation.Nonnull;

public enum HouseLevel {
    ;

    public static byte parse(final @Nonnull String string) {
        @Nonnull String upper = string.toUpperCase();

        if (upper.endsWith("EMELETES"))
            upper = upper.substring(0, upper.length() - 8);

        return (byte) Integer.parseInt(upper.trim());
    }

}
