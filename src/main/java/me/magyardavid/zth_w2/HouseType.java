package me.magyardavid.zth_w2;

import javax.annotation.Nonnull;

public enum HouseType {
    KERTES,
    TARSAS,
    PANEL;

    public static @Nonnull HouseType parse(final @Nonnull String string) {
        switch (string.trim().toLowerCase()) {
            case "kertes ház":
            case "kertesház":
            case "kertes":
                return KERTES;
            case "társas ház":
            case "társasház":
            case "társas":
                return TARSAS;
            case "panel":
            case "panelház":
            case "panel ház":
                return PANEL;
            default:
                return valueOf(string);
        }
    }
}
